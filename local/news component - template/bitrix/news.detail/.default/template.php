<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
$arPageDataCommon = \Itech\Newpage::getInstance()->getData('common');
$arPageDataSocial = unserialize($arPageDataCommon['PROPS']['UF_SOCIAL']);
?>

<main>
    <div class="page news-element-page">
        <div class="news-element">
            <div class="news-element__mainblock">
                <img class="news-element__leaf-1" src="/frontend/dist/assets/img/partners-leaf-2.png">
                <img class="news-element__leaf-2" src="/frontend/dist/assets/img/news-element-1.png">
                <img class="news-element__leaf-3" src="/frontend/dist/assets/img/news-element-2.png">
                <img class="news-element__leaf-4" src="/frontend/dist/assets/img/news-element-3.png">
                <div class="news-element__nav">
                    <?$APPLICATION->IncludeComponent(
                        "bitrix:breadcrumb",
                        "breadcrumb",
                        array(
                            "PATH" => "",
                            "SITE_ID" => "s1",
                            "START_FROM" => "0",
                            "COMPONENT_TEMPLATE" => "breadcrumb"
                        ),
                        false
                    );?>
                </div>
                <h1 class="h1"><?=$arResult["NAME"]?></h1>
                <div class="news-element__content">
                    <div class="news-element__information">
                        <p class="date"><?=str_replace('.', '/', $arResult["DISPLAY_ACTIVE_FROM"])?></p>
                        <div class="news-element__socials">
                            <a class="news-element__social" href="<?=$arPageDataSocial["FB"]?>">
                                <div class="news-element__social-facebook"></div>
                            </a>
                            <a class="news-element__social" href="<?=$arPageDataSocial["INST"]?>">
                                <div class="news-element__social-instagram"></div>
                            </a>
                            <a class="news-element__social" href="<?=$arPageDataSocial["YT"]?>">
                                <div class="news-element__social-youtube"></div>
                            </a>
                        </div>
                    </div>
                    <?=$arResult["DETAIL_TEXT"]?>
                    <div class="news-element__image">
                        <?if (!empty($arResult["DETAIL_PICTURE"]["SRC"])):?>
                            <img src="<?=$arResult["DETAIL_PICTURE"]["SRC"]?>">
                        <?endif?>
                    </div>

                    <?if (!empty($arResult["PROPERTIES"]["FILES"])):?>
                        <div class="news-element__files">
                            <?
                            $arSelect = Array("NAME", "PROPERTY_FILE", "PROPERTY_DESC", "PROPERTY_SIZE");
                            $arFilter = Array("IBLOCK_ID"=>13, "ELEMENT_ID"=>$arResult["PROPERTIES"]["FILES"]["VALUE"], "ACTIVE"=>"Y");
                            $res = CIBlockElement::GetList(Array(), $arFilter, false, [], $arSelect);
                            while($ob = $res->GetNextElement())
                            {
                                $arFields = $ob->GetFields();?>
                                <a class="news-element__file" download href="<?=CFile::GetPath($arFields["PROPERTY_FILE_VALUE"])?>">
                                    <p class="news-element__file-name"><?=$arFields["NAME"]?></p>
                                    <div class="news-element__file-block">
                                        <img class="news-element__file-image" src="/frontend/dist/assets/img/pdf-file.png">
                                        <div class="news-element__file-info">
                                            <p class="news-element__file-text"><?=$arFields["PROPERTY_DESC_VALUE"]?></p>
                                            <p class="news-element__file-text">Файл: <?=$arFields["PROPERTY_SIZE_VALUE"]?></p>
                                        </div>
                                    </div>
                                </a>
                            <?}?>
                        </div>
                    <?endif?>
                </div>
            </div>
        </div>
