<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
$i = 0;

$count_items = get_object_vars($arResult["NAV_RESULT"])["NavRecordCount"];
$pages = ceil($count_items / $arParams["NEWS_COUNT"]);
?>

<main>
    <div class="page news-page">
        <div class="news-title"><img class="news-title__leaf-1" src="/frontend/dist/assets/img/news-leaf-1.png"><img class="news-title__leaf-2" src="/frontend/dist/assets/img/news-leaf-2.png"><img class="news-title__leaf-3" src="/frontend/dist/assets/img/news-leaf-3.png"><img class="news-title__leaf-4" src="/frontend/dist/assets/img/news-leaf-4.png">
            <div class="news-title__mainblock">
                <div class="news-title__nav">
                    <?$APPLICATION->IncludeComponent(
                        "bitrix:breadcrumb",
                        "breadcrumb",
                        array(
                            "PATH" => "",
                            "SITE_ID" => "s1",
                            "START_FROM" => "0",
                            "COMPONENT_TEMPLATE" => "breadcrumb"
                        ),
                        false
                    );?>
                </div>
                <h1 class="h1"><?=$APPLICATION->ShowTitle()?></h1>
            </div>
        </div>
        <div class="company-news">
            <div class="company-news__mainblock">
                <img class="company-news__leaf-1" src="/frontend/dist/assets/img/reviews-leaf-1.png">

                <?foreach($arResult["ITEMS"] as $arItem):?>
                    <?
                    $i++;
                    $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
                    $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
                    ?>

                    <?if ($i == 1):?>
                        <div class="company-news__main-container">
                            <div class="company-news-main">
                                <img class="company-news-main__image" src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>">
                                <div class="company-news-main__content">
                                    <p class="date"><?=str_replace('.', ' / ', $arItem["DISPLAY_ACTIVE_FROM"])?></p>
                                    <p class="company-news-main__title"><?=$arItem["NAME"]?></p>
                                    <p class="company-news-main__text"><?=$arItem["PREVIEW_TEXT"]?></p>
                                    <a class="offer-link" href="<?=$arItem["DETAIL_PAGE_URL"]?>">
                                        <div class="offer-link__line"></div>
                                        <p class="offer-link__linktext">Подробнее</p>
                                    </a>
                                </div>
                            </div>
                        </div>
                    <?endif?>


                    <?if ($i == 2 || $i == 3 || $i == 4):?>
                        <?if ($i == 2):?>
                            <div class="company-news__container">
                        <?endif?>

                        <a class="company-news__item" href="<?=$arItem["DETAIL_PAGE_URL"]?>">
                            <img class="company-news__image" src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>">
                            <p class="date"><?=str_replace('.', ' / ', $arItem["DISPLAY_ACTIVE_FROM"])?></p>
                            <p class="company-news__text"><?=$arItem["NAME"]?></p>
                        </a>

                        <?if ($i == 4 || $i == count($arResult["ITEMS"])):?>
                            </div>
                        <?endif?>
                    <?endif?>


                    <?if ($i == 5):?>
                        <div class="company-news__main-container">
                            <div class="company-news-main-reverse">
                                <img class="company-news-main__image" src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>">
                                <div class="company-news-main__content-reverse">
                                    <p class="date"><?=str_replace('.', ' / ', $arItem["DISPLAY_ACTIVE_FROM"])?></p>
                                    <p class="company-news-main__title"><?=$arItem["NAME"]?></p>
                                    <p class="company-news-main__text"><?=$arItem["PREVIEW_TEXT"]?></p>
                                    <a class="offer-link" href="<?=$arItem["DETAIL_PAGE_URL"]?>">
                                        <div class="offer-link__line"></div>
                                        <p class="offer-link__linktext">Подробнее</p>
                                    </a>
                                </div>
                            </div>
                        </div>
                    <?endif?>

                    <?if ($i > 5):?>
                        <?if ($i == 6):?>
                            <div class="company-news__container">
                        <?endif?>
                                <a class="company-news__item" href="<?=$arItem["DETAIL_PAGE_URL"]?>">
                                    <img class="company-news__image" src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>">
                                    <p class="date"><?=str_replace('.', ' / ', $arItem["DISPLAY_ACTIVE_FROM"])?></p>
                                    <p class="company-news__text"><?=$arItem["NAME"]?></p>
                                </a>
                        <?if ($i == count($arResult["ITEMS"])):?>
                            </div>
                        <?endif?>
                    <?endif?>
                <?endforeach;?>
            </div>
        </div>

        <?if ($pages != 1):?>

            <div class="pagination">

                <?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
                    <?=$arResult["NAV_STRING"];?>
                <?endif;?>

                <?$num_p = $_GET["PAGEN_1"] ?? '1';?>

                <form>
                    <div class="pagination__mobile">
                        <button class="small-arrow small-arrow_left" name = "PAGEN_1" value="<?=$num_p - 1?>"
                                <?=($num_p <= 1) ? 'disabled' : ''?>>
                            <div class="small-arrow-left-image"></div>
                        </button>
                        <p class="partners-navigation__pages"><?=$num_p;?> - <?=$pages?></p>
                        <button class="small-arrow small-arrow_right" name = "PAGEN_1" value="<?=$num_p + 1?>"
                            <?=($num_p >= $pages) ? 'disabled' : ''?>>
                            <div class="small-arrow-right-image"></div>
                        </button>
                    </div>
                </form>

            </div>

        <?endif?>
    </div>
</main>


