<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("FES-AGRO");

$arPageData = \Itech\Newpage::getInstance()->getData('/');
$arPageDataMain = unserialize($arPageData['PROPS']['UF_MAIN']);
$arPageDataImgText = unserialize($arPageData['PROPS']['UF_IMG_TEXT']);
$arPageDataCompany = unserialize($arPageData['PROPS']['UF_COMPANY']);
$arPageDataPartners = unserialize($arPageData['PROPS']['UF_PARTNERS']);
$arPageDataProd = unserialize($arPageData['PROPS']['UF_PROD']);
$arPageDataServ = unserialize($arPageData['PROPS']['UF_SERV']);
$arPageDataStocks = unserialize($arPageData['PROPS']['UF_STOCKS']);
$arPageDataNews = unserialize($arPageData['PROPS']['UF_NEWS']);

$arPageDataCommon = \Itech\Newpage::getInstance()->getData('common');
$arPageDataSocial = unserialize($arPageDataCommon['PROPS']['UF_SOCIAL']);
?>
    <main>
        <div class="page main-page">
            <div class="description">
                <div class="description__mainblock">
                    <p class="secondary"><?=$arPageDataMain["MINI"]?></p>
                    <h1 class="h1"><?=$arPageDataMain["MAIN"]?></h1>
                </div>
            </div>
            <div class="about">
                <div class="about__container">
                    <div class="about-numbers">
                        <div class="about-numbers__item">
                            <p class="about-numbers__primary"><?=$arPageDataImgText["A"]?></p>
                            <p class="about-numbers__secondary"><?=$arPageDataImgText["B"]?></p>
                        </div>
                        <div class="about-numbers__item">
                            <p class="about-numbers__primary"><?=$arPageDataImgText["C"]?></p>
                            <p class="about-numbers__secondary"><?=$arPageDataImgText["D"]?></p>
                        </div>
                        <div class="about-numbers__item">
                            <p class="about-numbers__primary"><?=$arPageDataImgText["E"]?></p>
                            <p class="about-numbers__secondary"><?=$arPageDataImgText["F"]?></p>
                        </div>
                        <div class="about-numbers__item">
                            <p class="about-numbers__primary"><?=$arPageDataImgText["G"]?></p>
                            <p class="about-numbers__secondary"><?=$arPageDataImgText["H"]?></p>
                        </div>
                    </div>
                </div>
                <div class="about-socials">
                    <a class="about-socials__item" href="<?=$arPageDataSocial["YT"]?>">
                        <div class="about-socials__background"></div>
                        <img class="about-socials__social" src="/frontend/dist/assets/img/youtube.svg">
                    </a>
                    <a class="about-socials__item" href="<?=$arPageDataSocial["FB"]?>">
                        <div class="about-socials__background"></div>
                        <img class="about-socials__social" src="/frontend/dist/assets/img/facebook.svg">
                    </a>
                    <a class="about-socials__item" href="<?=$arPageDataSocial["INST"]?>">
                        <div class="about-socials__background"></div>
                        <img class="about-socials__social" src="/frontend/dist/assets/img/instagram.svg">
                    </a>
                </div>
                <a class="about__leaf" href="#aboutCompany" id="scroll-leaf">
                    <img src="/frontend/dist/assets/img/about-green-button.png">
                </a>
            </div>
            <div class="about-numbers-min">
                <div class="about-numbers-min__item">
                    <p class="about-numbers-min__primary"><?=$arPageDataImgText["A"]?></p>
                    <p class="about-numbers-min__secondary"><?=$arPageDataImgText["B"]?></p>
                </div>
                <div class="about-numbers-min__item">
                    <p class="about-numbers-min__primary"><?=$arPageDataImgText["C"]?></p>
                    <p class="about-numbers-min__secondary"><?=$arPageDataImgText["D"]?></p>
                </div>
                <div class="about-numbers-min__item">
                    <p class="about-numbers-min__primary"><?=$arPageDataImgText["E"]?></p>
                    <p class="about-numbers-min__secondary"><?=$arPageDataImgText["F"]?></p>
                </div>
                <div class="about-numbers-min__item">
                    <p class="about-numbers-min__primary"><?=$arPageDataImgText["G"]?></p>
                    <p class="about-numbers-min__secondary"><?=$arPageDataImgText["H"]?></p>
                </div>
            </div>
            <div class="today" id="aboutCompany"><img class="today__leaf" src="/frontend/dist/assets/img/partners-leaf-2.svg">
                <div class="today__mainblock">
                    <p class="secondary"><?=$arPageDataCompany["MINI"]?></p>
                    <h2 class="h2 today__header"><?=$arPageDataCompany["TITLE"]?></h2>
                    <p class="text"><?=$arPageDataCompany["DESC"]?></p>
                    <a href="<?=$arPageDataCompany["BUT_LINK"]?>" class="button"><?=$arPageDataCompany["BUT_TEXT"]?></a>
                </div>
                <div class="today-images__container">
                    <div class="today-images">
                        <img class="today-images__image today-images__image_1" src="<?=CFile::GetPath($arPageData['PROPS']['UF_COMP_IMG1'])?>">
                        <img class="today-images__image today-images__image_2" src="<?=CFile::GetPath($arPageData['PROPS']['UF_COMP_IMG2'])?>">
                        <img class="today-images__image today-images__image_3" src="<?=CFile::GetPath($arPageData['PROPS']['UF_COMP_IMG3'])?>">
                    </div>
                </div>
            </div>
            <div class="partners">
                <img class="partners__leaf" src="/frontend/dist/assets/img/partners-leaf.svg">
                <img class="partners__leaf-mobile" src="/frontend/dist/assets/img/partners-leaf-mobile.svg">
                <div class="partners__mainblock">
                    <div class="partners__title">
                        <p class="secondary"><?=$arPageDataPartners["MINI"]?></p>
                        <h2 class="h2"><?=$arPageDataPartners["TITLE"]?></h2>
                    </div>

                    <div class="partners-table__container">
                        <div class="partners-table">
                            <?$APPLICATION->IncludeComponent("bitrix:news.list", "partners", Array(
                                "ACTIVE_DATE_FORMAT" => "d.m.Y",	// Формат показа даты
                                "ADD_SECTIONS_CHAIN" => "N",	// Включать раздел в цепочку навигации
                                "AJAX_MODE" => "N",	// Включить режим AJAX
                                "AJAX_OPTION_ADDITIONAL" => "",	// Дополнительный идентификатор
                                "AJAX_OPTION_HISTORY" => "N",	// Включить эмуляцию навигации браузера
                                "AJAX_OPTION_JUMP" => "N",	// Включить прокрутку к началу компонента
                                "AJAX_OPTION_STYLE" => "N",	// Включить подгрузку стилей
                                "CACHE_FILTER" => "N",	// Кешировать при установленном фильтре
                                "CACHE_GROUPS" => "Y",	// Учитывать права доступа
                                "CACHE_TIME" => "36000000",	// Время кеширования (сек.)
                                "CACHE_TYPE" => "A",	// Тип кеширования
                                "CHECK_DATES" => "Y",	// Показывать только активные на данный момент элементы
                                "DETAIL_URL" => "",	// URL страницы детального просмотра (по умолчанию - из настроек инфоблока)
                                "DISPLAY_BOTTOM_PAGER" => "N",	// Выводить под списком
                                "DISPLAY_DATE" => "N",	// Выводить дату элемента
                                "DISPLAY_NAME" => "N",	// Выводить название элемента
                                "DISPLAY_PICTURE" => "N",	// Выводить изображение для анонса
                                "DISPLAY_PREVIEW_TEXT" => "N",	// Выводить текст анонса
                                "DISPLAY_TOP_PAGER" => "N",	// Выводить над списком
                                "FIELD_CODE" => array(	// Поля
                                    0 => "PREVIEW_PICTURE",
                                    1 => "",
                                ),
                                "FILTER_NAME" => "",	// Фильтр
                                "HIDE_LINK_WHEN_NO_DETAIL" => "N",	// Скрывать ссылку, если нет детального описания
                                "IBLOCK_ID" => "8",	// Код информационного блока
                                "IBLOCK_TYPE" => "main",	// Тип информационного блока (используется только для проверки)
                                "INCLUDE_IBLOCK_INTO_CHAIN" => "N",	// Включать инфоблок в цепочку навигации
                                "INCLUDE_SUBSECTIONS" => "N",	// Показывать элементы подразделов раздела
                                "MESSAGE_404" => "",	// Сообщение для показа (по умолчанию из компонента)
                                "NEWS_COUNT" => "0",	// Количество новостей на странице
                                "PAGER_BASE_LINK_ENABLE" => "N",	// Включить обработку ссылок
                                "PAGER_DESC_NUMBERING" => "N",	// Использовать обратную навигацию
                                "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",	// Время кеширования страниц для обратной навигации
                                "PAGER_SHOW_ALL" => "N",	// Показывать ссылку "Все"
                                "PAGER_SHOW_ALWAYS" => "N",	// Выводить всегда
                                "PAGER_TEMPLATE" => ".default",	// Шаблон постраничной навигации
                                "PAGER_TITLE" => "Новости",	// Название категорий
                                "PARENT_SECTION" => "",	// ID раздела
                                "PARENT_SECTION_CODE" => "",	// Код раздела
                                "PREVIEW_TRUNCATE_LEN" => "",	// Максимальная длина анонса для вывода (только для типа текст)
                                "PROPERTY_CODE" => array(	// Свойства
                                    0 => "",
                                    1 => "",
                                ),
                                "SET_BROWSER_TITLE" => "N",	// Устанавливать заголовок окна браузера
                                "SET_LAST_MODIFIED" => "N",	// Устанавливать в заголовках ответа время модификации страницы
                                "SET_META_DESCRIPTION" => "N",	// Устанавливать описание страницы
                                "SET_META_KEYWORDS" => "N",	// Устанавливать ключевые слова страницы
                                "SET_STATUS_404" => "N",	// Устанавливать статус 404
                                "SET_TITLE" => "N",	// Устанавливать заголовок страницы
                                "SHOW_404" => "N",	// Показ специальной страницы
                                "SORT_BY1" => "ACTIVE_FROM",	// Поле для первой сортировки новостей
                                "SORT_BY2" => "SORT",	// Поле для второй сортировки новостей
                                "SORT_ORDER1" => "DESC",	// Направление для первой сортировки новостей
                                "SORT_ORDER2" => "ASC",	// Направление для второй сортировки новостей
                                "STRICT_SECTION_CHECK" => "N",	// Строгая проверка раздела для показа списка
                            ),
                                false
                            );?>
                        </div>
                    </div>
                    <div class="partners-navigation__container">
                        <div class="partners-navigation">
                            <button class="small-arrow small-arrow_left swiper-button-prev">
                                <div class="small-arrow-left-image"></div>
                            </button>
                            <p class="partners-navigation__pages"></p>
                            <button class="small-arrow small-arrow_right swiper-button-next">
                                <div class="small-arrow-right-image"></div>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="services">
                <div class="services__mainblock"><img class="services__leaf" src="/frontend/dist/assets/img/leaf4.png">
                    <p class="secondary"><?=$arPageDataProd["MINI"]?></p>
                    <h2 class="h2"><?=$arPageDataProd["TITLE"]?></h2>
                    <div class="product product_hidden" id="product1"><img class="product__arrow" src="/frontend/dist/assets/img/arrow-vertical.svg"><img class="product__image" src="/frontend/dist/assets/img/product1.svg">
                        <div class="product__name">
                            <p class="product__text">Средства<br>защиты растений</p>
                        </div>
                        <div class="product__menu product__menu_first"><a class="product__item" href="#">Гербициты</a><a class="product__item" href="#">Инсектициды</a><a class="product__item" href="#">Нематициды</a><a class="product__item" href="#">Фунгициды</a><a class="product__item" href="#">Акарициды</a><a class="product__item" href="#">Родентициды</a></div>
                        <div class="product__mini-menu">
                            <div class="product__mini-col"><a class="product__item" href="#">Гербициты</a><a class="product__item" href="#">Инсектициды</a><a class="product__item" href="#">Нематициды</a></div>
                            <div class="product__mini-col"><a class="product__item" href="#">Фунгициды</a><a class="product__item" href="#">Акарициды</a><a class="product__item" href="#">Родентициды</a></div>
                        </div>
                    </div>
                    <div class="product product_hidden" id="product2"><img class="product__arrow" src="/frontend/dist/assets/img/arrow-vertical.svg"><img class="product__image" src="/frontend/dist/assets/img/product2.svg">
                        <div class="product__name">
                            <p class="product__text">Удобрения<br>и микроудобрения</p>
                        </div>
                        <div class="product__menu product__menu_second"><a class="product__item" href="#">НПК+микро</a><a class="product__item" href="#">Простые соли</a><a class="product__item" href="#">Антистрессанты, аминокислоты</a><a class="product__item" href="#">Регуляторы роста</a><a class="product__item" href="#">Инокулянты</a><a class="product__item" href="#">Родентициды</a></div>
                        <div class="product__mini-menu">
                            <div class="product__mini-col"><a class="product__item" href="#">НПК+микро</a><a class="product__item" href="#">Инокулянты</a><a class="product__item" href="#">Регуляторы роста</a></div>
                            <div class="product__mini-col"><a class="product__item" href="#">Простые соли</a><a class="product__item" href="#">Родентициды</a><a class="product__item" href="#">Антистрессанты, аминокислоты</a></div>
                        </div>
                    </div>
                    <div class="product product_hidden" id="product3"><img class="product__arrow" src="/frontend/dist/assets/img/arrow-vertical.svg"><img class="product__image" src="/frontend/dist/assets/img/product3.svg">
                        <div class="product__name">
                            <p class="product__text">Семена</p>
                        </div>
                        <div class="product__menu product__menu_third"><a class="product__item" href="#">Подсолнечники</a><a class="product__item" href="#">Кукуруза</a><a class="product__item" href="#">Сахарная свекла</a><a class="product__item" href="#">Рапс</a><a class="product__item" href="#">Соя</a><a class="product__item" href="#">Овощи</a></div>
                        <div class="product__mini-menu">
                            <div class="product__mini-col"><a class="product__item" href="#">Подсолнечники</a><a class="product__item" href="#">Кукуруза</a><a class="product__item" href="#">Сахарная свекла</a></div>
                            <div class="product__mini-col"><a class="product__item" href="#">Рапс</a><a class="product__item" href="#">Соя</a><a class="product__item" href="#">Овощи</a></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="solutions">
                <div class="solutions__block-container">
                    <div class="solutions__title">
                        <p class="secondary"><?=$arPageDataServ["MINI"]?></p>
                        <h2 class="h2"><?=$arPageDataServ["TITLE"]?></h2>
                    </div>
                    <div class="solutions__container">
                        <div class="solutions__mainblock">
                            <button class="big-arrow big-arrow_left swiper-button-prev-2">
                                <div class="big-arrow-left-image"></div>
                            </button>
                            <div class="solutions__slider">
                                <div class="swiper-container-2">
                                    <div class="swiper-wrapper">

                                        <?$APPLICATION->IncludeComponent("bitrix:news.list", "services_block", Array(
                                            "ACTIVE_DATE_FORMAT" => "d.m.Y",	// Формат показа даты
                                            "ADD_SECTIONS_CHAIN" => "N",	// Включать раздел в цепочку навигации
                                            "AJAX_MODE" => "N",	// Включить режим AJAX
                                            "AJAX_OPTION_ADDITIONAL" => "",	// Дополнительный идентификатор
                                            "AJAX_OPTION_HISTORY" => "N",	// Включить эмуляцию навигации браузера
                                            "AJAX_OPTION_JUMP" => "N",	// Включить прокрутку к началу компонента
                                            "AJAX_OPTION_STYLE" => "N",	// Включить подгрузку стилей
                                            "CACHE_FILTER" => "N",	// Кешировать при установленном фильтре
                                            "CACHE_GROUPS" => "Y",	// Учитывать права доступа
                                            "CACHE_TIME" => "36000000",	// Время кеширования (сек.)
                                            "CACHE_TYPE" => "A",	// Тип кеширования
                                            "CHECK_DATES" => "Y",	// Показывать только активные на данный момент элементы
                                            "DETAIL_URL" => "",	// URL страницы детального просмотра (по умолчанию - из настроек инфоблока)
                                            "DISPLAY_BOTTOM_PAGER" => "N",	// Выводить под списком
                                            "DISPLAY_DATE" => "N",	// Выводить дату элемента
                                            "DISPLAY_NAME" => "N",	// Выводить название элемента
                                            "DISPLAY_PICTURE" => "N",	// Выводить изображение для анонса
                                            "DISPLAY_PREVIEW_TEXT" => "N",	// Выводить текст анонса
                                            "DISPLAY_TOP_PAGER" => "N",	// Выводить над списком
                                            "FIELD_CODE" => array(	// Поля
                                                0 => "PREVIEW_PICTURE",
                                                1 => "NAME",
                                                2 => "PREVIEW_TEXT",
                                            ),
                                            "FILTER_NAME" => "",	// Фильтр
                                            "HIDE_LINK_WHEN_NO_DETAIL" => "N",	// Скрывать ссылку, если нет детального описания
                                            "IBLOCK_ID" => "9",	// Код информационного блока
                                            "IBLOCK_TYPE" => "main",	// Тип информационного блока (используется только для проверки)
                                            "INCLUDE_IBLOCK_INTO_CHAIN" => "N",	// Включать инфоблок в цепочку навигации
                                            "INCLUDE_SUBSECTIONS" => "N",	// Показывать элементы подразделов раздела
                                            "MESSAGE_404" => "",	// Сообщение для показа (по умолчанию из компонента)
                                            "NEWS_COUNT" => "0",	// Количество новостей на странице
                                            "PAGER_BASE_LINK_ENABLE" => "N",	// Включить обработку ссылок
                                            "PAGER_DESC_NUMBERING" => "N",	// Использовать обратную навигацию
                                            "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",	// Время кеширования страниц для обратной навигации
                                            "PAGER_SHOW_ALL" => "N",	// Показывать ссылку "Все"
                                            "PAGER_SHOW_ALWAYS" => "N",	// Выводить всегда
                                            "PAGER_TEMPLATE" => ".default",	// Шаблон постраничной навигации
                                            "PAGER_TITLE" => "Новости",	// Название категорий
                                            "PARENT_SECTION" => "",	// ID раздела
                                            "PARENT_SECTION_CODE" => "",	// Код раздела
                                            "PREVIEW_TRUNCATE_LEN" => "",	// Максимальная длина анонса для вывода (только для типа текст)
                                            "PROPERTY_CODE" => array(	// Свойства
                                                0 => "",
                                                1 => "",
                                            ),
                                            "SET_BROWSER_TITLE" => "N",	// Устанавливать заголовок окна браузера
                                            "SET_LAST_MODIFIED" => "N",	// Устанавливать в заголовках ответа время модификации страницы
                                            "SET_META_DESCRIPTION" => "N",	// Устанавливать описание страницы
                                            "SET_META_KEYWORDS" => "N",	// Устанавливать ключевые слова страницы
                                            "SET_STATUS_404" => "N",	// Устанавливать статус 404
                                            "SET_TITLE" => "N",	// Устанавливать заголовок страницы
                                            "SHOW_404" => "N",	// Показ специальной страницы
                                            "SORT_BY1" => "ACTIVE_FROM",	// Поле для первой сортировки новостей
                                            "SORT_BY2" => "SORT",	// Поле для второй сортировки новостей
                                            "SORT_ORDER1" => "DESC",	// Направление для первой сортировки новостей
                                            "SORT_ORDER2" => "ASC",	// Направление для второй сортировки новостей
                                            "STRICT_SECTION_CHECK" => "N",	// Строгая проверка раздела для показа списка
                                        ),
                                            false
                                        );?>
                                    </div>
                                </div>
                            </div>
                            <button class="big-arrow big-arrow_right swiper-button-next-2">
                                <div class="big-arrow-right-image"></div>
                            </button>
                        </div>
                        <div class="solutions__nav">
                            <button class="small-arrow small-arrow_left swiper-button-prev-2">
                                <div class="small-arrow-left-image"></div>
                            </button>
                            <p class="partners-navigation__pages">01 - 02</p>
                            <button class="small-arrow small-arrow_right swiper-button-next-2">
                                <div class="small-arrow-right-image"></div>
                            </button>
                        </div>
                        <a class="button" href="<?=$arPageDataServ["BUT_LINK"]?>"><?=$arPageDataServ["BUT_TEXT"]?></a>
                    </div>
                    <div class="solutions__background"></div>
                </div>
            </div>
            <div class="offers">
                <div class="offers__mainblock"><img class="offers__leaf-1" src="/frontend/dist/assets/img/leaf3.png"><img class="offers__leaf-2" src="/frontend/dist/assets/img/leaf5.png">
                    <p class="secondary"><?=$arPageDataStocks["MINI"]?></p>
                    <h2 class="h2"><?=$arPageDataStocks["TITLE"]?></h2>
                    <div class="offers-content">

                        <?
                        $i = 1;
                        $arSelect = Array("NAME", "DISPLAY_ACTIVE_FROM", "ACTIVE", "PREVIEW_PICTURE", "PREVIEW_TEXT", "DETAIL_PAGE_URL", "PROPERTY_MAIN");
                        $arFilter = Array("IBLOCK_ID"=>15, "ACTIVE"=>"Y", 'PROPERTY_MAIN_VALUE' => 'Показывать на главной');
                        $res = CIBlockElement::GetList(Array('ID' => 'DESC'), $arFilter, false, Array("nPageSize"=>2), $arSelect);
                        while($ob = $res->GetNextElement())
                        {
                            $arFields = $ob->GetFields();
                            $i++ == 1 ? $cls = 'left' : $cls = 'right';?>

                            <div class="offers-content__block offers-content__block_<?=$cls?>">
                                <div class="offers__date"><?=str_replace('.', ' / ', $arFields["DISPLAY_ACTIVE_FROM"])?></div>
                                <div class="offer">
                                    <a href="<?=$arFields["DETAIL_PAGE_URL"]?>">
                                        <img src="<?=CFile::GetPath($arFields["PREVIEW_PICTURE"])?>">
                                    </a>
                                    <p class="offer__title"><?=$arFields["NAME"]?></p>
                                    <p class="offer__text"><?=$arFields["PREVIEW_TEXT"]?></p>
                                    <a class="offer-link" href="<?=$arFields["DETAIL_PAGE_URL"]?>">
                                        <div class="offer-link__line"></div>
                                        <p class="offer-link__linktext">Подробнее</p>
                                    </a>
                                </div>
                            </div>
                        <?}?>
                    </div>
                </div>
            </div>
            <div class="news">
                <div class="news__container">
                    <div class="news__mainblock">
                        <p class="news__title_secondary"><?=$arPageDataNews["MINI"]?></p>
                        <p class="news__title_primary"><?=$arPageDataNews["TITLE"]?></p>


                        <?$APPLICATION->IncludeComponent(
                            "bitrix:news.list",
                            "main_news",
                            array(
                                "ACTIVE_DATE_FORMAT" => "d.m.Y",
                                "ADD_SECTIONS_CHAIN" => "N",
                                "AJAX_MODE" => "N",
                                "AJAX_OPTION_ADDITIONAL" => "",
                                "AJAX_OPTION_HISTORY" => "N",
                                "AJAX_OPTION_JUMP" => "N",
                                "AJAX_OPTION_STYLE" => "N",
                                "CACHE_FILTER" => "N",
                                "CACHE_GROUPS" => "Y",
                                "CACHE_TIME" => "36000000",
                                "CACHE_TYPE" => "A",
                                "CHECK_DATES" => "Y",
                                "DETAIL_URL" => "",
                                "DISPLAY_BOTTOM_PAGER" => "N",
                                "DISPLAY_DATE" => "N",
                                "DISPLAY_NAME" => "N",
                                "DISPLAY_PICTURE" => "N",
                                "DISPLAY_PREVIEW_TEXT" => "N",
                                "DISPLAY_TOP_PAGER" => "N",
                                "FIELD_CODE" => array(
                                    0 => "NAME",
                                    1 => "PREVIEW_TEXT",
                                    2 => "DATE_ACTIVE_FROM",
                                    3 => "",
                                ),
                                "FILTER_NAME" => "",
                                "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                                "IBLOCK_ID" => "12",
                                "IBLOCK_TYPE" => "news",
                                "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                                "INCLUDE_SUBSECTIONS" => "N",
                                "MESSAGE_404" => "",
                                "NEWS_COUNT" => "3",
                                "PAGER_BASE_LINK_ENABLE" => "N",
                                "PAGER_DESC_NUMBERING" => "N",
                                "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                                "PAGER_SHOW_ALL" => "N",
                                "PAGER_SHOW_ALWAYS" => "N",
                                "PAGER_TEMPLATE" => ".default",
                                "PAGER_TITLE" => "Новости",
                                "PARENT_SECTION" => "",
                                "PARENT_SECTION_CODE" => "",
                                "PREVIEW_TRUNCATE_LEN" => "",
                                "PROPERTY_CODE" => array(
                                    0 => "",
                                    1 => "",
                                ),
                                "SET_BROWSER_TITLE" => "N",
                                "SET_LAST_MODIFIED" => "N",
                                "SET_META_DESCRIPTION" => "N",
                                "SET_META_KEYWORDS" => "N",
                                "SET_STATUS_404" => "N",
                                "SET_TITLE" => "N",
                                "SHOW_404" => "N",
                                "SORT_BY1" => "ACTIVE_FROM",
                                "SORT_BY2" => "SORT",
                                "SORT_ORDER1" => "DESC",
                                "SORT_ORDER2" => "ASC",
                                "STRICT_SECTION_CHECK" => "N",
                                "COMPONENT_TEMPLATE" => ".default"
                            ),
                            false
                        );?>

                    </div>
                </div>
            </div>
        </div>
    </main>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>